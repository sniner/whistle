# Whistle

A simple whistle that you can put on your key ring or attach to a cord.

Published on [Thingiverse][1] on 17.07.2020.

License: [CC BY 4.0][2]

[1]: https://www.thingiverse.com/thing:4545101
[2]: https://creativecommons.org/licenses/by/4.0/