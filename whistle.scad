// Length of the whistle without loop
length = 40; //[20:100]

// Width of both sides of the whistle
width = 10; //[6:20]

// Must be less or equal to half of the width
fillet_radius = 2; //[0:20]


module korpus_aussen(laenge=60, dicke=10, rundung=1.0)
{
    if (rundung>0) {
        distanz = dicke/2-rundung;
        if (distanz<=0) {
            translate([0, 0, laenge/2])
            cylinder(d=dicke, h=laenge, center=true);
        } else {
            linear_extrude(laenge)
            hull() {
                translate([-distanz,  distanz, 0]) circle(r=rundung);
                translate([ distanz,  distanz, 0]) circle(r=rundung);
                translate([ distanz, -distanz, 0]) circle(r=rundung);
                translate([-distanz, -distanz, 0]) circle(r=rundung);
            }
        }
    } else {
        translate([0, 0, laenge/2])
        cube([dicke, dicke, laenge], center=true);
    }
}

module korpus(laenge=60, dicke=10, wandstaerke=1.0, rundung=2.0)
{
    difference() {
        korpus_aussen(laenge=laenge, dicke=dicke, rundung=rundung);
        translate([0, 0, laenge/2])
        cylinder(d=dicke-2*wandstaerke, h=laenge+2*wandstaerke, center=true);
    }
}

module windkanal(laenge=10, dicke=10, oeffnung=1.0, rundung=2.0)
{
    intersection() {
        korpus_aussen(laenge=laenge, dicke=dicke, rundung=rundung);
        translate([-dicke/2, dicke/2, 0])
        rotate([90, 0, 0])
        linear_extrude(dicke)
        polygon([
            [dicke-oeffnung, 0],
            [0, laenge],
            [0, 0],
        ]);
    }
}

module schneidekante(laenge=6, dicke=10, toleranz=1, tiefe=4)
{
    translate([dicke/2-tiefe, -(dicke+toleranz)/2, 0])
    rotate([-90, 0, 0])
    linear_extrude(dicke+toleranz)
    polygon([
        [0, 0],
        [tiefe+toleranz, (tiefe+toleranz)*laenge/tiefe],
        [tiefe+toleranz, 0],
    ]);
}

module pfeife(laenge=50, dicke=10, windkanal_laenge=10, rundung=2)
{
    difference() {
        union() {
            korpus(laenge=laenge, dicke=dicke, rundung=rundung);
            translate([0, 0, laenge-windkanal_laenge])
            windkanal(laenge=windkanal_laenge, dicke=dicke, oeffnung=0.3*dicke, rundung=rundung);
        }
        translate([0, 0, laenge-windkanal_laenge])
        schneidekante(laenge=0.6*dicke, dicke=dicke, tiefe=0.4*dicke);
    }
}

module oese_seitenanschnitt(hoehe, rundung, dicke)
{
    translate([dicke/2, 0, 0])
    rotate([0, -90, 0])
    linear_extrude(dicke)
    hull() {
        translate([rundung, 0, 0]) circle(r=rundung);
        translate([hoehe+rundung, 0, 0]) circle(r=rundung);
    }
}

module oese(dicke=10, steg=4, rundung=2)
{
    hoehe = dicke + steg/2;
    anschnitt_r = (dicke-steg)/2;
    loch_r = (dicke-steg)/2;
    difference() {
        intersection() {
            difference() {
                korpus_aussen(laenge=hoehe, dicke=dicke, rundung=rundung);
                translate([0, anschnitt_r+steg/2, steg/2])
                oese_seitenanschnitt(hoehe=hoehe, rundung=anschnitt_r, dicke=2*dicke);
                translate([0, -anschnitt_r-steg/2, steg/2])
                oese_seitenanschnitt(hoehe=hoehe, rundung=anschnitt_r, dicke=2*dicke);
            }
            union() {
                translate([0, dicke/2, hoehe-dicke/2])
                rotate([90, 0, 0])
                cylinder(d=dicke, h=dicke);
                korpus_aussen(laenge=hoehe-dicke/2, dicke=dicke, rundung=rundung);
            }
        }
        translate([0, dicke/2, hoehe-loch_r-steg/2])
        rotate([90, 0, 0])
        cylinder(r=loch_r, h=dicke);
    }
}

module pfeife_mit_oese(laenge=40, dicke=10, rundung=2)
{
    rotate([0, 0, -90]) {
        translate([0, 0, laenge])
        rotate([180, 0, 0])
        pfeife(laenge=laenge, dicke=dicke, rundung=rundung);
        translate([0, 0, laenge])
        rotate([0, 0, 0])
        oese(dicke=dicke, rundung=rundung);
    }
}

pfeife_mit_oese(laenge=length, dicke=width, rundung=fillet_radius, $fn=24);